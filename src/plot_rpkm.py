#rpkm Plot Programm
import pandas as pan
import matplotlib.pyplot as plot
import seaborn as sea
import numpy as nump
import cmath

#I/O imports
import os
import sys

#own modules
import file_operations as fo


def run(filename,ycutoff):

    file = fo.open_input_file(filename)

    #error log file
    error_filename = "seq_rpkm_error.txt"
    error_log = fo.create_error_log(error_filename)


    #Get position of rpkm in file
    firstLine = next(file)
    pos = fo.findColumnPos(firstLine,"RPKM")
    num_columns = fo.num_used_att(firstLine)

    #Read file and gather rpkm
    simple_seq_arr = []
    fo.fill_data_array(simple_seq_arr,file,error_log,firstLine,num_columns,pos,float)
    seq_rpkm = nump.array(simple_seq_arr)
    
    
    #Create DataFrame
    seq_rpkm_frame = pan.DataFrame({'RPKM':seq_rpkm})

    #variables for plotting
    number_of_bins = 75 #Maybe give this to user, but 75 is for the moment the best value
    
    #Create Plot
    rpkm_hist = seq_rpkm_frame.hist(bins= number_of_bins)
    if(ycutoff > 0):
        plot.ylim(0,ycutoff)

    #show plot and close file
    plot.show()
    file.close
