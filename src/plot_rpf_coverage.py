import pandas as pan
import matplotlib.pyplot as plot
import seaborn as sea
import numpy as nump

#I/O imports
import os
import sys

#own modules
import file_operations as fo


def run(filename):
    
    file = fo.open_input_file(filename)

    #error log file
    error_filename = "seq_rpf_coverage_error.txt"
    error_log = fo.create_error_log(error_filename)

    #Get position of rpf Coverage in file
    firstLine = next(file)
    pos = fo.findColumnPos(firstLine,"RPF coverage")
    num_columns = fo.num_used_att(firstLine)

    #Read file and gather sequence lengths
    simple_seq_arr = []
    fo.fill_data_array(simple_seq_arr,file,error_log,firstLine,num_columns,pos,float)
    seq_rpf_coverage = nump.array(simple_seq_arr)
  
    #Create DataFrame
    seq_rpf_coverage_frame = pan.DataFrame({'RPF coverage':seq_rpf_coverage})

    #Create Plot
    seq_plot = seq_rpf_coverage_frame.groupby('RPF coverage').size().plot(title='RPF coverage distribution')
    seq_plot.locator_params(Integer=True) # Number of Sequences in Integer

    #Plot format options
    seq_plot.set_ylabel("Sequence RPF coverage Frequency")

    #show plot and close file
    plot.show()
    file.close()

