import pandas as pan
import matplotlib.pyplot as plot
import seaborn as sea
import numpy as nump

#I/O imports
import os
import sys

#own modules
import file_operations as fo


def run(ycutoff):
    this_path = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(this_path,'..','output',"iupred_results.txt")

    try:
        file = open(file_path,"r")

    except:
        print("ipred_results.txt in output not found!")
    
    finally:
        #error log file
        error_filename = "disorder_error.txt"
        error_log = fo.create_error_log(error_filename)

        #Get position of AA sequence disorder in file
        firstLine = next(file)
        pos = fo.findColumnPos(firstLine,"AA sequence disorder")
        num_columns = fo.num_used_att(firstLine)

        #Read file and gather disorder value
        simple_seq_arr = []
        fo.fill_data_array(simple_seq_arr,file,error_log,firstLine,num_columns,pos,float)
        seq_rpf_coverage = nump.array(simple_seq_arr)
        
        #Create DataFrame
        seq_rpf_coverage_frame = pan.DataFrame({'Disorder':seq_rpf_coverage})

        #Create Plot
        seq_plot = seq_rpf_coverage_frame.groupby('Disorder').size().plot(title='Disorder distribution')
        seq_plot.locator_params(Integer=True) # Number of Sequences in Integer

        #Plot format options
        seq_plot.set_ylabel("Sequence Disorder Frequency")

        #ylim
        if(ycutoff > 0):
            plot.ylim(0,ycutoff)
            
        #show plot and close file
        plot.show()
        file.close()

