#TKinter
import tkinter
import tkinter.filedialog
from tkinter import messagebox

#import file coppy
import shutil
import os
#import files for plotting
import plot_annotation
import plot_rpf_coverage
import plot_rpkm
import plot_disorder
import plot_sequence_length
import file_operations as fo

#date
import datetime



#(button) functions
def exe_iupred():
    answer = messagebox.askokcancel("Confirm","Running this can take a long time, "+
    "especially on larger files, continue?")
    if(answer):
        filename = "results.txt"
        if(whatFileSelecter.get() == 2):
            filename = "results_filtered.txt"
        file = fo.open_input_file(filename)
        output_file = fo.create_output_file("iupred_results.txt")
        error_log = fo.create_error_log("iupred_error.txt")
        fo.create_iupred_annotated_file(file,output_file,error_log)

#Executes all selected filters
def exe_filter():
    filename = "results.txt"
    file = fo.open_input_file(filename)
    firstLine = next(file)
    this_path = os.path.dirname(os.path.abspath(__file__))
   
    #What file to use?
    WhatFile = whatFileSelecter.get()
    #1 orignial 2 filtered file
    if(WhatFile == 1): #we copy result into filtered for easier process
        filename = "results.txt"
        filename_filtered = "results_filtered.txt"
        fo.create_filtered_input_file(filename_filtered)
        src = os.path.join(this_path,'..','input',filename)
        dest = os.path.join(this_path,'..','input',filename_filtered)
        shutil.copy(src,dest)
        filename = filename_filtered
        filename_filtered = "temp.txt"
           
    else:
        
        filename = "results_filtered.txt"
        filename_filtered = "temp.txt"
        fo.create_filtered_input_file(filename_filtered)
        
    error_log = fo.create_error_log("filter_error.txt")
    
    dest = os.path.join(this_path,'..','input',filename)
    src = os.path.join(this_path,'..','input',filename_filtered)

    #sequence length
    if(filter_seg_length_checked.get() == 1):
        filtered_File = fo.create_filtered_input_file(filename_filtered)
        filtered_File.write(firstLine)
        file = open(dest,"r")
        pos = fo.findColumnPos(firstLine,'Sorf length')
        num_columns = fo.num_used_att(firstLine)
        min = minLength.get()
        max = maxLength.get()
        words = []
        next(file)
        fo.fill_file_filtered(filtered_File,file,error_log,firstLine,num_columns,pos,int,min,max,words)

        file.close()
        filtered_File.close()
        shutil.copy(src,dest)

    #annotation
    if(filter_annotation_checked.get() == 1):
        filtered_File = fo.create_filtered_input_file(filename_filtered)
        filtered_File.write(firstLine)
        file = open(dest,"r")

        pos = fo.findColumnPos(firstLine,'Annotation')
        num_columns = fo.num_used_att(firstLine)
        words = []
        min = 0
        max = 0
        if(sORF_checker.get() == 1):
            words.append("sORF")
        if(exonic_checker.get()==1):
            words.append("exonic")
        if(intronic_checker.get() == 1):
            words.append("intronic")
        if(uTR5_checker.get()==1):
            words.append("5UTR")
        if(uTR3_checker.get() == 1):
            words.append("3UTR")
        if(pseudogene_checker.get()== 1):
            words.append("pseudogene")
        if(lcrna_checker.get() == 1):
            words.append("lncrna")
        if(intergenic_checker.get() == 1):
            words.append("intergenic")
        
        next(file)
        fo.fill_file_filtered(filtered_File,file,error_log,firstLine,num_columns,pos,str,min,max,words)

        file.close()
        filtered_File.close()
        shutil.copy(src,dest)

    
    #rpf coverage
    if(filter_rpf_coverage_checked.get() == 1):
        filtered_File = fo.create_filtered_input_file(filename_filtered)
        filtered_File.write(firstLine)
        file = open(dest,"r")

        pos = fo.findColumnPos(firstLine,'RPF coverage')
        num_columns = fo.num_used_att(firstLine)
        min = min_rpf.get()
        max = max_rpf.get()
        words = []
        next(file)
        fo.fill_file_filtered(filtered_File,file,error_log,firstLine,num_columns,pos,float,min,max,words)
        file.close()
        filtered_File.close()
        shutil.copy(src,dest)
       
       
    #rpkm
    if(filter_rpkm_checked.get() == 1):
        filtered_File = fo.create_filtered_input_file(filename_filtered)
        filtered_File.write(firstLine)
        file = open(dest,"r")

        pos = fo.findColumnPos(firstLine,'RPKM')
        num_columns = fo.num_used_att(firstLine)
        min = min_rpkm.get()
        max = max_rpkm.get()
        words = []
        next(file)
        fo.fill_file_filtered(filtered_File,file,error_log,firstLine,num_columns,pos,float,min,max,words)
        file.close()
        filtered_File.close()
        shutil.copy(src,dest)
       
#Save the filtered file
#if empty => date as filename   
def save_file():
    
    if not save_file_name.get():  
        filename= str(datetime.datetime.now())
     
    else:
        filename = save_file_name.get()
    
    filename = filename + ".txt"

   
    this_path = os.path.dirname(os.path.abspath(__file__))
    dest = os.path.join(this_path,'..','output',filename)
    src = os.path.join(this_path,'..','input',"results_filtered.txt")

    shutil.copy(src,dest)  

def load_file():
    #openfile dialog
    this_path = os.path.dirname(os.path.abspath(__file__))
    location = os.path.join(this_path,'..','output')
    load_file_path = tkinter.filedialog.askopenfilename(initialdir = location, title = "Select file",filetypes = [("text files","*.txt")]) 
    if(load_file_path == ""): #selection canceled
        return

    src = str(load_file_path)
    dest = os.path.join(this_path,'..','input',"results_filtered.txt")
    
    #copy loaded file into results_filtered.txt
    shutil.copy(src,dest)

def exe_disorder_plot():
    answer = messagebox.askokcancel("Confirm","Shows disorder plot of most recent iupred annotated file, "+
    "make sure to run iupred on the desired file beforehand!")
    if(answer):
        ycutoff =  disorder_cutoff.get()
        plot_disorder.run(ycutoff)

def exe_seq_length_plot():
    filename = "results.txt"
    if(whatFileSelecter.get() == 2):
        filename = "results_filtered.txt"
    plot_sequence_length.run(filename)

def exe_annotation_plot():
    filename = "results.txt"
    if(whatFileSelecter.get() == 2):
        filename = "results_filtered.txt"
    plot_annotation.run(filename)

def exe_rpf_coverage_plot():
    filename = "results.txt"
    if(whatFileSelecter.get() == 2):
        filename = "results_filtered.txt"
    plot_rpf_coverage.run(filename)

def exe_rpkm_plot():
  
    filename = "results.txt"
    if(whatFileSelecter.get() == 2):
        filename = "results_filtered.txt"
    ycutoff = rpkm_cutoff.get()
    plot_rpkm.run(filename,ycutoff)
 
def exe_annotation_check():
    answer = messagebox.askokcancel("Confirm","Running this can take a VERY long time "+
    "(a single line can take half a second), continue?")
    if(answer):
        filename = "results.txt"
        if(whatFileSelecter.get() == 2):
            filename = "results_filtered.txt"
        file= fo.open_input_file(filename)
        
        this_path = os.path.dirname(os.path.abspath(__file__))
        gtf_dir_path = os.path.join(this_path,"..","gtf_files")
        gtf_file_path = tkinter.filedialog.askopenfilename(initialdir = gtf_dir_path, title = "Select gtf file",filetypes = [("gtf file","*.gtf")]) 
        if(gtf_file_path == ""): #selection canceled
            return

        db_dir_path = os.path.join(this_path, "blastdb")
        db_file_path = tkinter.filedialog.askopenfilename(initialdir = db_dir_path, title = "Select Database",filetypes = [("database","*.nhr")])
        if(db_file_path == ""): #selection canceled
            return
        db_help = os.path.basename(db_file_path)
        db_name = os.path.splitext(db_help)[0]

        errorlog = fo.create_error_log("annotation_check_error.txt")
        
        #Str = "das ist ein string"
        evalue_value = evalue.get()
                
        fo.check_file_annotation(file,gtf_file_path,errorlog,db_name,evalue_value)

        errorlog.close() 
        file.close()

def exe_transcript_id_check():
    answer = messagebox.askokcancel("Confirm","Running this can take a long time, "+
    "especially on larger files, continue?")
    if(answer):
        filename = "results.txt"
        if(whatFileSelecter.get() == 2):
            filename = "results_filtered.txt"
        fo.transcript_id_check(filename)
 
def exe_makedb():
    answer = messagebox.askokcancel("Confirm","Running this can take a moment, "+
    "old database files also get deleted, continue?")
    if(answer):
        fo.makeblastdb()

#Save of the filtered txt as a fasta
def exe_save_to_fasta():

    if not save_file_name.get():  
        name= str(datetime.datetime.now())
     
    else:
        name = save_file_name.get()
   
    this_path = os.path.dirname(os.path.abspath(__file__))
    f_name = name + ".fasta"    
    #create query fasta file
    fasta_file_output_path = os.path.join(this_path,"..","output",f_name)
    fasta = open(fasta_file_output_path,"w+") 

    filtered_input_file_path = os.path.join(this_path,"..","input","results_filtered.txt")
    filtered_file = open(filtered_input_file_path)
    firstline = next(filtered_file)

    pos_SorfID = fo.findColumnPos(firstline,"Sorf ID")
    pos_Chromosome = fo.findColumnPos(firstline,"Chromosome")
    pos_Strand = fo.findColumnPos(firstline,"Strand")
    pos_Seq_Start = fo.findColumnPos(firstline, "Sorf start")	
    pos_Seq_End = fo.findColumnPos(firstline,"Sorf end") 
    pos_Seq_length =fo.findColumnPos(firstline,"Sorf length")
    pos_annotiation = fo.findColumnPos(firstline,"Annotation")
    pos_id = fo.findColumnPos(firstline,"Ensembl transcript ID")
    pos_sequence = fo.findColumnPos(firstline,"AA sequence")
    
    for nextLine in filtered_file:
        lineArr = nextLine.split()
        fasta.write(">"+lineArr[pos_SorfID]+ " " + lineArr[pos_Chromosome] + " " + lineArr[pos_Strand] + " " 
        + lineArr[pos_Seq_Start] + " " + lineArr[pos_Seq_length] + " " + lineArr[pos_Seq_End]
        +" " + lineArr[pos_annotiation]+ " "+ lineArr[pos_id] +"\n"    )
        #Sequence schreiben
        sequence = lineArr[pos_sequence]
        sequenceArr = [ sequence[i:i+75] for i in range(0, len(sequence), 75) ]
        for line in sequenceArr:
            fasta.write(line+"\n")
    
    filtered_file.close()
    fasta.close()
    

# --- Build window & Elements ---

#MainMenu
mainMenu= tkinter.Tk()
mainMenu.title = "Main Menu"   #Dont Work #Fixing later on

#Buttons   
button_seq_plot = tkinter.Button(mainMenu, text="sequence length plot", command = exe_seq_length_plot )
button_rpf_coverage = tkinter.Button(mainMenu, text="RPF coverage plot",command = exe_rpf_coverage_plot)
button_annotation_plot = tkinter.Button(mainMenu, text="annotation plot", command = exe_annotation_plot)
button_rpkm_plot = tkinter.Button(mainMenu, text="RPKM distribution plot", command = exe_rpkm_plot)
button_trans_id = tkinter.Button(mainMenu, text= "Transcript ID cross-check", command = exe_transcript_id_check)
button_iupred = tkinter.Button(mainMenu, text= "Run iupred on File", command = exe_iupred)
button_filter = tkinter.Button(mainMenu, text= "Run filter", command = exe_filter)
button_save = tkinter.Button(mainMenu,text = "save (filtered) results", command = save_file)
button_update_dabase = tkinter.Button(mainMenu,text = "update (local) BLAST Database", command = exe_makedb)
button_load = tkinter.Button(mainMenu,text = "load file", command = load_file)
button_annotation_check = tkinter.Button(mainMenu, text = "Check annotation",command =  exe_annotation_check)
button_save_to_fasta = tkinter.Button(mainMenu,text = "Save to fasta",command = exe_save_to_fasta)
button_disorder_plot = tkinter.Button(mainMenu, text = "Disorder plot" , command = exe_disorder_plot)

#seqlength Filter Stuff
filter_seg_length_checked = tkinter.IntVar()
checkbox_filter_seq_length = tkinter.Checkbutton(mainMenu,text= "Length Filter", variable = filter_seg_length_checked)
minLength = tkinter.IntVar()
minLength_entry = tkinter.Entry(mainMenu,width = 6, textvariable = minLength )
maxLength = tkinter.IntVar()
maxLength_entry = tkinter.Entry(mainMenu,width = 6, textvariable = maxLength)

#annotation filter stuff
filter_annotation_checked = tkinter.IntVar()
checkbox_filter_annotation = tkinter.Checkbutton(mainMenu, text = "annotation filter", variable = filter_annotation_checked)


annotation_MB = tkinter.Menubutton(mainMenu,text = "select", relief= "raised")
annotation_MB.menu = tkinter.Menu(annotation_MB, tearoff = 0)
annotation_MB["menu"] = annotation_MB.menu

sORF_checker = tkinter.IntVar()
exonic_checker = tkinter.IntVar()
intronic_checker = tkinter.IntVar()
uTR5_checker  = tkinter.IntVar()
uTR3_checker = tkinter.IntVar()
pseudogene_checker  = tkinter.IntVar()
lcrna_checker  = tkinter.IntVar()
intergenic_checker = tkinter.IntVar()

annotation_MB.menu.add_checkbutton(label = "sORF", variable = sORF_checker)
annotation_MB.menu.add_checkbutton(label = "exonic", variable = exonic_checker)
annotation_MB.menu.add_checkbutton(label = "intronic", variable = intronic_checker)
annotation_MB.menu.add_checkbutton(label = "5UTR", variable =uTR5_checker )
annotation_MB.menu.add_checkbutton(label = "3UTR", variable =uTR3_checker )
annotation_MB.menu.add_checkbutton(label = "pseudgene", variable = pseudogene_checker )
annotation_MB.menu.add_checkbutton(label = "lcrna", variable = lcrna_checker )
annotation_MB.menu.add_checkbutton(label = "intergenic", variable = intergenic_checker )

#rpf coverage filter stuff
filter_rpf_coverage_checked = tkinter.IntVar()
checkbox_filter_rpf_coverage = tkinter.Checkbutton(mainMenu,text = "rpf coverage filter", variable = filter_rpf_coverage_checked)
min_rpf = tkinter.DoubleVar()
min_rpf_entry = tkinter.Entry(mainMenu,width = 6, textvariable = min_rpf )
max_rpf = tkinter.DoubleVar()
max_rpf_entry = tkinter.Entry(mainMenu,width = 6, textvariable = max_rpf)

#rpkm coverage filter stuff
filter_rpkm_checked = tkinter.IntVar()
checkbox_filter_rpkm = tkinter.Checkbutton(mainMenu, text = "rpkm filter", variable = filter_rpkm_checked)
min_rpkm = tkinter.DoubleVar()
min_rpkm_entry = tkinter.Entry(mainMenu,width = 6, textvariable = min_rpkm )
max_rpkm = tkinter.DoubleVar()
max_rpkm_entry = tkinter.Entry(mainMenu,width = 6, textvariable = max_rpkm)

#wich file to use?
whatFileSelecter = tkinter.IntVar()
whatFileSelecter.set(1)
file_Radiobutton = tkinter.Radiobutton(mainMenu, text = "original File", variable = whatFileSelecter, value = 1)
file_Radiobutton2 = tkinter.Radiobutton(mainMenu, text = "filtered File", variable = whatFileSelecter, value = 2)

#save  File stuff
save_file_name = tkinter.StringVar()
save_file_name_entry = tkinter.Entry(mainMenu, width = 12, textvariable = save_file_name)

#load file stuff
#atm just the button init in the button section

#rpkm ycutoff stuff
rpkm_cutoff  = tkinter.IntVar()
rpkm_cutoff_entry = tkinter.Entry(mainMenu,width = 5, textvariable = rpkm_cutoff)

#evalue
evalue = tkinter.DoubleVar()
evalue.set(1.0)
evalue_entry = tkinter.Entry(mainMenu , width = 5, textvariable = evalue)

#disorder y cut off
disorder_cutoff = tkinter.IntVar()
disorder_cutoff_entry = tkinter.Entry(mainMenu, width = 5, textvariable = disorder_cutoff)

# ---   add Elements   ----

#grid Buttons
button_seq_plot.grid(row = 8) 
button_annotation_plot.grid(row = 9)
button_rpf_coverage.grid(row = 10)
button_rpkm_plot.grid(row = 11)

button_filter.grid( row = 18)
button_save_to_fasta.grid(row = 5,column = 4)

button_trans_id.grid(row = 21)
button_iupred.grid(row = 22)
button_disorder_plot.grid(row = 22, columns = 3)
button_update_dabase.grid(row = 23) 
button_annotation_check.grid(row = 24)

#SECTIONS
tkinter.Label(text = "Awesome_sORF", font = "Helvetica 20 bold").grid(row = 0)
tkinter.Label(text = "      ").grid(row = 1, columns = 1)

tkinter.Label(text = "      ").grid(row = 6, columns = 1)
tkinter.Label(text = "Plot Area", font = "Helvetica 15 bold").grid(row = 7, columns = 1)

tkinter.Label(text = "      ").grid(row = 12, columns = 1)
tkinter.Label(text = "Filter Area", font = "Helvetica 15 bold").grid(row = 13, columns = 1)

tkinter.Label(text = "      ").grid(row = 19, columns = 1)
tkinter.Label(text = "Execute" , font = "Helvetica 15 bold").grid(row = 20, columns =1)


#Save File Stuff
button_save.grid( row = 5, column = 2)
tkinter.Label(mainMenu,text ="Name of file:").grid(row =5, column = 0)
save_file_name_entry.grid(row =5, column = 1)

#Load File Stuff
button_load.grid( row = 5, column = 3 )

#rpkm ycutoff
rpkm_cutoff_entry.grid( row  = 11 , column = 1)
tkinter.Label(mainMenu,text = "y-axis max value").grid(row = 11 , column = 2)

#grid Filter length
checkbox_filter_seq_length.grid(row = 14,sticky="W", column = 0)
tkinter.Label(mainMenu,text = "Min:").grid(row = 14, column = 1)
minLength_entry.grid(row = 14, column = 2)
tkinter.Label(mainMenu,text = "Max:").grid(row = 14, column = 3)
maxLength_entry.grid(row = 14, column =4 )

#grid annotation
checkbox_filter_annotation.grid(row = 15, sticky = "W")
annotation_MB.grid(row = 15, column = 2)

#grid rpf_coverage 
checkbox_filter_rpf_coverage.grid(row = 16, sticky ="W")
tkinter.Label(mainMenu,text = "Min:").grid(row = 16, column = 1)
min_rpf_entry.grid(row = 16, column = 2)
tkinter.Label(mainMenu,text = "Max:").grid(row = 16, column = 3)
max_rpf_entry.grid(row = 16, column =4 )

#grid rpkm
checkbox_filter_rpkm.grid(row = 17, sticky = "W")
tkinter.Label(mainMenu,text = "Min:").grid(row = 17, column = 1)
min_rpkm_entry.grid(row = 17, column = 2)
tkinter.Label(mainMenu,text = "Max:").grid(row = 17, column = 3)
max_rpkm_entry.grid(row = 17, column =4 )

#what file to use?
tkinter.Label(mainMenu,text = "Which File?").grid(row = 4, column =0)
file_Radiobutton.grid(row = 4, column = 1)
file_Radiobutton2.grid(row =4,column = 2)

#evalue entry
tkinter.Label(mainMenu, text = "e value").grid(row = 24,column = 2)
evalue_entry.grid(row = 24, column = 1)

#disorder y cut off
disorder_cutoff_entry.grid(row = 22 , column = 2)
tkinter.Label(mainMenu, text = "y-axis max value").grid(row = 22, column = 3)

mainMenu.mainloop()

