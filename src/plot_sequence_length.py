#Modules for Plotting seq length
import pandas as pan
import matplotlib.pyplot as plot
import seaborn as sea
import numpy as nump

#I/O imports
import os
import sys

#own modules
import file_operations as fo


def run(filename):
    file = fo.open_input_file(filename)

    #error log file
    error_filename = "seq_length_error.txt"
    error_log = fo.create_error_log(error_filename)

    #Get position of sequence length in file
    firstLine = next(file)
    pos = fo.findColumnPos(firstLine,'Sorf length')
    num_columns = fo.num_used_att(firstLine)

    #Read file and gather sequence lengths
    simple_seq_arr = []
    fo.fill_data_array(simple_seq_arr,file,error_log,firstLine,num_columns,pos,int)
    seq_lengths = nump.array(simple_seq_arr)

    #Create DataFrame
    seq_lengths_frame = pan.DataFrame({'Sequence Length':seq_lengths})

    #Create Plot
    seq_plot = seq_lengths_frame.groupby('Sequence Length').size().plot(title='Sequence length distribution')
    seq_plot.locator_params(integer=True) # Number of Sequences in Integer

    #Plot format options
    seq_plot.set_ylabel("Sequence Length Frequency")

    #show plot and close file
    plot.show()
    file.close()

