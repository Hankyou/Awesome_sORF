#annotation Plot Programm
import pandas as pan
import matplotlib.pyplot as plot
import seaborn as sea
import numpy as nump

#I/O imports
import os
import sys

#own modules
import file_operations as fo


def run(filename):

    file= fo.open_input_file(filename)

    #error log file
    error_filename = "seq_annotation_error.txt"
    error_log = fo.create_error_log(error_filename)

    #Get position of annotation in file
    firstLine = next(file)
    pos = fo.findColumnPos(firstLine,'Annotation')
    num_columns = fo.num_used_att(firstLine)

    #Read file and gather annotation
    seq = []
    fo.fill_data_array(seq,file,error_log,firstLine,num_columns,pos,str)

    #Creating Result Array + Index 
    res = [0,0,0,0,0,0,0,0]
    index = 1 

    #Process FileData
    for ano in seq:
        if("sORF" in ano):
            res[0] += 1
        elif("exonic" in ano):
            res[1] += 1
        elif("intronic" in ano):
            res[2] += 1
        elif("5UTR" in ano):
            res[3] += 1
        elif("3UTR"  in ano):
            res[4] += 1
        elif("pseudogene" in ano):
            res[5] += 1 
        elif("lncrna"  in ano):
            res[6] += 1 
        elif("intergenic" in ano):
            res[7] += 1   
    
        index +=1

    #creation plot
    seq_annotations = pan.Series(res)
    df = pan.DataFrame({"annotation":["sORF","exonic","intronic","5UTR","3UTR","pseudogene","lncrna","intergenic"], "values": seq_annotations})
    ax = df.plot.bar(x = "annotation",y = "values", rot = 22, title = "Annotations")
    
    #@small annotation Values a fixed Values Kicks the annotation out of the frame
    additonValue = int (max(res)/100)

    #annotation for exact number over the bars #position tuned for expected values
    ax.annotate(res[0], xy=(0-0.1,res[0]+ additonValue))
    ax.annotate(res[1], xy=(1-0.3,res[1]+ additonValue))
    ax.annotate(res[2], xy=(2-0.25,res[2]+ additonValue))
    ax.annotate(res[3], xy=(3-0.25,res[3]+ additonValue))
    ax.annotate(res[4], xy=(4-0.25,res[4]+ additonValue))
    ax.annotate(res[5], xy=(5-0.1,res[5]+ additonValue))
    ax.annotate(res[6], xy=(6-0.2,res[6]+ additonValue))
    ax.annotate(res[7], xy=(7-0.2,res[7]+ additonValue))

    #show plot and close file
    plot.show()
    file.close()

