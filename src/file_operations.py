#Module file operations
#Defines functions for operating on files and other (computation-heavy) tasks

#TKinter
import tkinter

#I/O or other imports
import os
import sys
import subprocess
import tempfile
import stat
import collections
import time

#Iupred Import
import iupred.iupred2a as iupred2a

#Path of this file (used in some functions to get absolute path)
this_path = (os.path.dirname(os.path.abspath(__file__)))

#supported attributes (AA sequence disorder received from iupred)
supported_attributes = ['Sorf ID','Chromosome','Strand','Sorf end','Sorf length','Sorf start','Cell line',
'Start codon','Ensembl transcript ID','Annotation','AA sequence disorder','AA sequence','Transcript Sequence','RPKM',
'RPF coverage uniformity','RPF coverage','Biotype']

#stores (number of whitespaces) for each attribute in order of supported_attributes
attribute_whitespace_count = []
for x in supported_attributes:
    attribute_whitespace_count.append(len(x.split())-1)

#Global attributes

#---PROJECTION_FACTOR---
#Factor for projecting gtf entries into the dictionary (compressing the position space)
#higher value = faster dictionary creation and higher compression, but possibly longer search time within dict
#Advised to change to 10**2 or even 10**1 for checking long (>1000 lines) files
PROJECTION_FACTOR = 10**3

#class definitions [START]

class Blasthit(object):
    def __init__(self, qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, scr, sstrand, bitscore):
        self.qseqid, self.sseqid, self.pident, self.length, self.mismatch, self.gapopen, self.qstart, self.qend, self.sstart, self.send, self.scr, self.sstrand, self.bitscore =  qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, scr, sstrand, bitscore

#class definitions [END]


#function definitions [START]

#checks if it is possible to cast value as "type"
def typecheck(value,type):
    try:
        type(value)
        return True
    except ValueError:
        return False

#return number of attributes used in the file
def num_used_att(firstLine):
    num = 0
    invalidAttribute = False
    while(not invalidAttribute):
        if(firstLine == ""):
            return num
        invalidAttribute = True
        for attribute in supported_attributes:
            if firstLine.startswith(attribute):
                invalidAttribute = False
                #cut attribute off
                firstLine = (firstLine[len(attribute):]).lstrip()
                num += 1
            else:
                pass
    sys.exit("Format of file is wrong: Contains invalid attributes (see README.txt) [occured in function: num_used_att]")

#finds position/column of attribute in File [firstLine:= first line of file]
def findColumnPos(firstLine,searched_attribute):
    pos = 0
    invalidAttribute = False
    while(not invalidAttribute):
        invalidAttribute = True
        for attribute in supported_attributes:
            if firstLine.startswith(attribute):
                invalidAttribute = False
                if attribute == searched_attribute:
                    return pos
                else:
                    #cut attribute off
                    firstLine = (firstLine[len(attribute):]).lstrip()
                    pos += 1
            else:
                pass
    sys.exit("Format of file is wrong: Contains invalid attributes or does not contain desired attribute (see README.txt) [occured in function: findColumnPos]")

#finds position where attribute appears in firstLine (primarily to edit first line e.g add new attribute)
#similiar to findcolumnPos, but used for the first line
def find_header_pos(firstLine,searched_attribute):
    pos = 0
    invalidAttribute = False
    while(not invalidAttribute):
        invalidAttribute = True
        att_iterator = -1
        for attribute in supported_attributes:
            att_iterator += 1
            if firstLine.startswith(attribute):
                invalidAttribute = False
                if attribute == searched_attribute:
                    return (pos + attribute_whitespace_count[att_iterator])
                else:
                    #cut attribute off
                    firstLine = (firstLine[len(attribute):]).lstrip()
                    pos += (1+attribute_whitespace_count[att_iterator])
            else:
                pass
    sys.exit("Format of file is wrong: Contains invalid attributes or does not contain desired attribute (see README.txt) [occured in function: find_header_pos]")

#takes name of error file, deletes old file and return reader of new error log file
def create_error_log(filename):
    filepath = os.path.join(this_path,'..','error_log', filename)
    #remove old error log
    if os.path.isfile(filepath):
        os.remove(filepath)
    #create new error log
    if os.path.exists(os.path.join(this_path,'..','error_log')):
        error_log = open(filepath, 'a')
    else:
        sys.exit("file_operations: create_error_log: This Line should never be reached")

    error_log.write("Lines that have not been processed, due to invalid formatting:\n\n")
    return error_log

#takes name of output file, deletes old file and return reader of new file
def create_output_file(filename):
    filepath = os.path.join(this_path,'..','output', filename)
    #remove old error log
    if os.path.isfile(filepath):
        os.remove(filepath)
    #create new error log
    if os.path.exists(os.path.join(this_path,'..','output')):
        output_file = open(filepath, 'a')
    else:
        sys.exit("file_operations: create_output_file: This Line should never be reached")

    return output_file

#create a file for the lines accepted by the filter
def create_filtered_input_file(filename):
    filepath = os.path.join(this_path,'..','input', filename)
    #remove old error log
    if os.path.isfile(filepath):
        os.remove(filepath)
    #create new error log
    if os.path.exists(os.path.join(this_path,'..','input')):
        output_file = open(filepath, 'w')
    else:
        sys.exit("file_operations: create_filtered_input_file: This Line should never be reached")

    return output_file

#returns reader of input file (results.txt)
def open_input_file(filename):
    filepath = os.path.join(this_path,'..','input', filename)
    #check if file exists & open
    if not os.path.isfile(filepath):
        sys.exit("Missing " + filepath)
    elif os.path.isfile(filepath):
        return open(filepath, "r")
    else:
        sys.exit("file_operations: open_input_file: This Line should never be reached")

#Tries to add placeholders for missing Data to prevent errors when reading a line. 
#takes an line array (line.split()), length of the line and the first/header -line
#Take caution, since resulting line isn´t guaranteed to work or placeholders could be inserted at wrong place
def repair_lineArr(lineArr,line_length,firstLine):
    #Lines with "Annotation == intergenic" are missing Ensembl transcript ID and Biotype (in every case so far). Add placeholder to avoid errors
    if ('intergenic' in lineArr) & (len(lineArr) < line_length):
        if ('Ensembl transcript ID' in firstLine) & ('Biotype' in firstLine):
            if (findColumnPos(firstLine,'Ensembl transcript ID') < findColumnPos(firstLine,'Biotype')):
                lineArr.insert(findColumnPos(firstLine,'Ensembl transcript ID'),'noData')
                lineArr.insert(findColumnPos(firstLine,'Biotype'),'noData')
            else:
                lineArr.insert(findColumnPos(firstLine,'Biotype'),'noData')
                lineArr.insert(findColumnPos(firstLine,'Ensembl transcript ID'),'noData')
        elif ('Ensembl transcript ID' in firstLine):
            lineArr.insert(findColumnPos(firstLine,'Ensembl transcript ID'),'noData')
        elif ('Biotype' in firstLine):
            lineArr.insert(findColumnPos(firstLine,'Biotype'),'noData')

#fills an array with the desired attribute data from file (e.g all sequence lengths)
#! file reader(s) should start at first data line, pass the first (Head)Line as param !
def fill_data_array(array,file,error_log,firstLine,line_length,attribute_position,type):
    lines = 0
    bad_lines = 0
    for nextLine in file:
        lines += 1
        lineArr = nextLine.split()
        repair_lineArr(lineArr,line_length,firstLine)

        #Attribute missing / format of line ok ?
        if len(lineArr) != line_length:
            error_log.write((" ".join(lineArr) + "\n| Too many/little columns present ("+ str(len(lineArr)) +"). "
                "Number of columns in line should match number of attributes ("+ str(line_length) +") in first Line |\n\n"))
            bad_lines += 1
            continue

        nextVal = lineArr[attribute_position] #attribute must be in column "attribute_position"
        
        #Attribute wrong type ?
        if not typecheck(nextVal,type):
            error_log.write(" ".join(lineArr) + "\n| An attribute has wrong datatype |\n\n")
            bad_lines += 1
            continue

        array.append(type(nextVal))
    error_log.write("{} out of {} lines have not been processed ({}%)".format(bad_lines,lines,(bad_lines/lines)*100))

#fills the outputfile with data that are accepted by the filter value(number min and max or word)
#when filtered by a word we just pass (0,0) as min and max
#the rest is equal to fill_data_array
def fill_file_filtered(output_file,file,error_log,firstLine,line_length,attribute_position,type,filter_min, filter_max, filter_word):
    
    lines = 0
    bad_lines = 0
    for nextLine in file:
        lines += 1
        lineArr = nextLine.split()
        repair_lineArr(lineArr,line_length,firstLine)

        #Attribute missing / format of line ok ?
        if len(lineArr) != line_length:
            error_log.write((" ".join(lineArr) + "\n| Too many/little columns present ("+ str(len(lineArr)) +"). "
                "Number of columns in line should match number of attributes ("+ str(line_length) +") in first Line |\n\n"))
            bad_lines += 1
            continue

         
        nextVal = lineArr[attribute_position] #attribute must be in column "attribute_position"
        
        #Attribute wrong type ?
        if not typecheck(nextVal,type):
            error_log.write(" ".join(lineArr) + "\n| An attribute has wrong datatype |\n\n")
            bad_lines += 1
            continue
        #Use of the filter
        
       # print(nextVal)
        if(type == int or type == float):
            if(type(nextVal) >= filter_min and type(nextVal) <= filter_max):
                #print(nextVal)
                output_file.write(nextLine)
        if(type == str):
            if(nextVal in filter_word):
                output_file.write(nextLine)    
   
    if (lines > 0):
       error_log.write("{} out of {} lines have not been processed ({}%)".format(bad_lines,lines,(bad_lines/lines)*100))
    else:
       error_log.write("No lines have been processed, hence no errors occured (maybe too many filters?)")

#crosschecks all entries in file1 with all entries in file2. 
#! file reader(s) should start at first data line, pass the first (Head)Line as param !
#Parameters: pos=Position of Attribute to be checked in file 1/2 | length= number of columns in file 1/2 | type= type of attribute (e.g int, string...)
#firstLine= first Line of File 1/2
# Writes result in output_file
def crosscheck(file1, file2, firstLine1, firstLine2, pos1, pos2, line_length1, line_length2, type, output_file, error_log):
    lines1 = 0
    lines2 = 0
    bad_lines1 = 0
    bad_lines2 = 0
    matches = 0
    output_file.write("Matching entries:\n\n")
    file2_lines = file2.readlines()
    for nextLine1 in file1:
        lines1 += 1
        lineArr1 = nextLine1.split()
        repair_lineArr(lineArr1,line_length1,firstLine1)

        #Attribute missing / format of line ok ?
        if len(lineArr1) != line_length1:
            error_log.write((" ".join(lineArr1) + "\n| In File "+ file1.name +
                " | Too many/little columns present ("+ str(len(lineArr1)) +"). "
                "Number of columns in line should match number of attributes ("+ str(line_length1) +") in first Line |\n\n"))
            bad_lines1 += 1
            continue

        nextVal1 = lineArr1[pos1] #length must be in column "attribute_position"
        
        #Attribute wrong type ?
        if not typecheck(nextVal1,type):
            error_log.write(" ".join(lineArr1) + "\n| In File "+ file1.name +" | An attribute has wrong datatype |\n\n")
            bad_lines1 += 1
            continue

        #cross check Line with all lines from file2
        lines_counted = False
        for nextLine2 in file2_lines:
            if(not lines_counted):
                lines2 += 1
            lineArr2 = nextLine2.split()
            repair_lineArr(lineArr2,line_length2,firstLine2)

            #Attribute missing / format of line ok ?
            if len(lineArr2) != line_length2:
                error_log.write((" ".join(lineArr2) + "\n| In File "+ file2.name +
                    " | Too many/little columns present ("+ str(len(lineArr2)) +"). "
                    "Number of columns in line should match number of attributes ("+ str(line_length2) +") in first Line |\n\n"))
                if(not lines_counted):
                    bad_lines2 += 1
                continue

            nextVal2 = lineArr2[pos2] #length must be in column "attribute_position"

            #Attribute wrong type ?
            if not typecheck(nextVal2,type):
                error_log.write(" ".join(lineArr2) + "\n| In File "+ file2.name +" | An attribute has wrong datatype |\n\n")
                if(not lines_counted):
                    bad_lines2 += 1
                continue

            #comparisons += 1
            if (nextVal1 == nextVal2):
                matches += 1
                output_file.write("Match:\n"+
                    os.path.basename(file1.name) +": "+" ".join(lineArr1)+
                    "\n"+os.path.basename(file2.name) +": "+" ".join(lineArr2)+"\n\n")

        #Only count lines once
        lines_counted = True
            
    output_file.write(("{} Matches between "+ os.path.basename(file1.name)+" and "+os.path.basename(file2.name)).format(matches))
    error_log.write(("{} out of {} lines have not been processed in " + os.path.basename(file1.name) +" ({}%)\n").format(bad_lines1,lines1,(bad_lines1/lines1)*100))
    error_log.write(("{} out of {} lines have not been processed in "+ os.path.basename(file2.name) +" ({}%)").format(bad_lines2,lines2,(bad_lines2/lines2)*100))

#uses crosscheck function for result & transcript_id txt
def transcript_id_check(filename):
    file1 = open_input_file(filename)
    file2 = open_input_file("transcript_ids.txt")
    firstLine1 = next(file1)
    firstLine2 = "Ensembl transcript ID"
    pos1 = findColumnPos(firstLine1,"Ensembl transcript ID")
    pos2 = 0
    line_length1 = num_used_att(firstLine1)
    line_length2 = 1
    output_file = create_output_file("crosscheck_result.txt")
    error_log = create_error_log("crosscheck_error.txt")
    crosscheck(file1, file2, firstLine1, firstLine2, pos1, pos2, line_length1, line_length2, str, output_file, error_log)

#performs iupred on sequence
def iupred(sequence,pred_type):
    if pred_type not in ['short', 'long', 'glob']:
        sys.exit("Error: viable prediction type options for iupred are |short,long,glob|\n")
    iupred2_result = iupred2a.iupred(sequence, pred_type)

    return iupred2_result

#creates new file in output dir, where disorder per residue of sequence is added
#! In this case file reader should start at first line of file, including head line !
def create_iupred_annotated_file(file,output_file,error_log):

    print("Compute disorder value (using iupred)...")
    firstLine = next(file)
    attribute_position = findColumnPos(firstLine,"AA sequence")
    line_length = num_used_att(firstLine)
    lines = 0
    bad_lines = 0
    
    firstLineArr = firstLine.split()
    firstLineArr.insert(find_header_pos(firstLine,'AA sequence')+1,'AA sequence disorder')
    output_file.write((" ".join(firstLineArr))+"\n")

    for nextLine in file:
        #For testing purposes: Stop after certain number of lines
        #if(lines==500):
        #    break
        lines += 1
        lineArr = nextLine.split()
        repair_lineArr(lineArr,line_length,firstLine)

        #Attribute missing / format of line ok ?
        if len(lineArr) != line_length:
            error_log.write((" ".join(lineArr) + "\n| Too many/little columns present ("+ str(len(lineArr)) +"). "
                "Number of columns in line should match number of attributes ("+ str(line_length) +") in first Line |\n\n"))
            bad_lines += 1
            continue

        sequence = lineArr[attribute_position] #sequence must be in column "attribute_position"
        
        #Attribute wrong type ?
        if not typecheck(sequence,str):
            error_log.write(" ".join(lineArr) + "\n| An attribute has wrong datatype |\n\n")
            bad_lines += 1
            continue

        #perform iupred
        disorderArr = iupred(sequence,'long')[0]

        #add disorder values into line
        
        #Compute one Disorder Value out of disorder/residue
        n_disordered = 0
        for residue in disorderArr:
            if residue >= 0.5:
                n_disordered +=1
        disordered_total = (float(n_disordered) / len(disorderArr))
        lineArr.insert(attribute_position+1, str(disordered_total))    
        
        output_line = " ".join(lineArr)
        output_file.write(output_line+"\n")

    error_log.write("\n{} out of {} lines have not been processed ({}%)".format(bad_lines,lines,(bad_lines/lines)*100))
    print("Finished computing disorder values!")

#TODO not working, try calling like blast?
#performs tango on sequence
def tango(identifier,sequence):
    if sys.platform.startswith("linux"):
        exe_name =  "tango_x86_64_release"
    elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
        exe_name =  "tango_win.exe"
    else:
        sys.exit("OS not supported")

    tango_dir = os.path.join(this_path,'tango')
    exe_file_path = os.path.join(this_path,'tango', exe_name)
    #exe_file_path = exe_name

    #Confusing shit do we really need THAT?
    handle, out_path = tempfile.mkstemp(suffix=".txt")
    os.close(handle)
   
    subdir = os.path.dirname(out_path)
    starting_dir = os.getcwd()
    os.chdir(subdir)
    
    tmp_name = "Bjoern mag bananen"
    cmd = " ".join("%s %s ct='N' nt='N' ph='7' te='298' io='0.1' seq='%s'" %
           (exe_file_path, tmp_name, sequence))
    #print(cmd)
  
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    p.wait()

    try:
        with open(out_path) as out_file:
            print(out_file.readline())  # skip header
            print("T")
            agg_len = 0
            counter = 0
            for line in out_file:
                print("line: "+line)
                agg_prob = float(line.split()[5])
                if agg_prob > 5:
                    counter += 1
                else:
                    if counter >= 5:
                        agg_len += counter
                    counter = 0
            if counter >= 5:
                agg_len += counter
        agg_perc = agg_len / float(len(seq))
        os.remove(out_path)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        agg_perc = -1
    
    os.chdir(starting_dir)
    print(agg_perc)
    return agg_perc

    '''
    #tango parameters
    nt = "nt=\"N\""
    ct = "ct=\"N\""
    ph = "ph=\"7.4\""
    te = "te=\"303\""
    io = "io=\"0.05\""
    tf = "tf=\"0\""
    stab = "stab=\"-10\""
    exe_sequence = "seq=\""+sequence+"\""
    arguments = " ".join([identifier,nt,ct,ph,te,io,tf,stab,exe_sequence])

    #print(" ".join([exepath,identifier,nt,ct,ph,te,io,tf,stab,exe_sequence]))
    print(exe_file_path+" "+ arguments)

    #run tango
    #result = subprocess.run([exe_file_path,[identifier,nt,ct,ph,te,io,tf,stab,exe_sequence]], capture_output=True, env={'PATH': tango_dir})
    result = subprocess.run([exe_file_path,[identifier,nt,ct,ph,te,io,tf,stab,exe_sequence]], capture_output=True, check=False)
    #subprocess.call([exepath,[identifier,nt,ct,ph,te,io,tf,stab,exe_sequence]], shell=True)
    
    #print(result.stdout[2:(len(result.stdout)-2)])
    result_string = result.stdout.decode('utf-8')
    print(result_string)
    return result_string
    '''

#performs a tblastn search, returns a list with BlastHit Objects
#query_abs_path= absolute path to a fasta file, containing the query sequence
#database_name= name of the dabase, where the blast search is performed. A Database with that name must exit in the blastdb folder.
#evalue= cutoff value for Blast search results
def tblastn(query_abs_path,database_name,evalue):
    #outfmt = "6"

    #get directory and executable name for creating path
    if sys.platform.startswith("linux"):
        dir_name = "ncbi-blast-2.8.1+_linux"
        exe_name = "tblastn"
        outfmt = "\"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue sstrand bitscore\""
    elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
        dir_name = "ncbi-blast-2.8.1+_win"
        exe_name = "tblastn.exe"
        outfmt = "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue sstrand bitscore"
    else:
        sys.exit("OS not supported")

    blastdb_path = os.path.join(this_path, "blastdb")
    bin_path = os.path.join(this_path, dir_name, "bin")
    tblastn_path = os.path.join(bin_path, exe_name)
    output_file_name = "tblastn_output.txt"
   
    #@linux changing permissions of executable
    if sys.platform.startswith("linux"):   
        os.chmod(tblastn_path,757)

    #setting environment variables for blast search
    os.environ["BLASTDB"] = blastdb_path
    my_env = os.environ
    my_env["PATH"] = bin_path + my_env["PATH"]

    #go into output directory
    old_dir = os.getcwd()
    os.chdir(os.path.join(this_path,"..","output"))

    if sys.platform.startswith("linux"):
        command = " ".join([tblastn_path,
        "-db",database_name,"-query",query_abs_path,"-out",output_file_name,"-outfmt",outfmt,
        "-evalue",str(evalue)])
    elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
        command = [tblastn_path,
        "-db",database_name,"-query",query_abs_path,"-out",output_file_name,"-outfmt",outfmt,
        "-evalue",str(evalue)]

    p1 = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, env=my_env)

    #(output, err) = p1.communicate() #for trouble-shooting
    p1.wait()

    results = parse_blast(output_file_name)

    #return to old directory
    os.chdir(old_dir)
    
    #print(output.decode('utf-8',"replace")) #for trouble-shooting
    return results

#parses blast result file (-outfmt 6 + strand) and returns list of BlastHits
def parse_blast(results):
    hits = []
    with open(results, 'r') as myfile:
        for line in myfile.readlines():
            qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, scr, sstrand, bitscore = line.split('\t')
            hits.append(Blasthit(qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, scr, sstrand, bitscore))        
    return hits

#Creates Blast Databases for all fasta files
def makeblastdb():
    print("Updating Database...")
    #get directory and executable name for creating path
    if sys.platform.startswith("linux"):
        dir_name = "ncbi-blast-2.8.1+_linux"
        exe_name = "makeblastdb"
    elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
        dir_name = "ncbi-blast-2.8.1+_win"
        exe_name = "makeblastdb.exe"
    else:
        sys.exit("OS not supported")

    makedb_path = os.path.join(this_path, dir_name, "bin", exe_name)
    bin_path = os.path.join(this_path, dir_name, "bin")
    blastdb_path = os.path.join(this_path, "blastdb")

    if sys.platform.startswith("linux"):   
        os.chmod(makedb_path,757)

    my_env = os.environ
    my_env["PATH"] = bin_path + my_env["PATH"]

    #go into blastdb directory
    old_dir = os.getcwd()
    os.chdir(blastdb_path)

    #delete old files
    for old_db_file in os.listdir(blastdb_path):
        if not "gitkeep" in old_db_file:
            os.remove(old_db_file)

    #create DB of all files
    fasta_dir_path = os.path.join(this_path, "..", "fasta_files")
    for file_name in os.listdir(fasta_dir_path):
        if(file_name.endswith(".fasta") or file_name.endswith(".fa") or file_name.endswith(".mpfa") or
        file_name.endswith(".fna") or file_name.endswith(".fsa")):
            fasta_file = file_name
            db_name = os.path.splitext(file_name)[0]+"_DB"
        else:
            continue

        fasta_file_path = os.path.join(fasta_dir_path, fasta_file)

        #create db for fasta file
        if sys.platform.startswith("linux"):
            command = " ".join([makedb_path,"-dbtype","nucl","-in",fasta_file_path,"-out",db_name])
        elif sys.platform.startswith("win") or sys.platform.startswith("cygwin"):
            command = [makedb_path,"-dbtype","nucl","-in",fasta_file_path,"-out",db_name]
        
        p1 = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, env=dict(my_env, BLASTDB=blastdb_path))

        #(output, err) = p1.communicate() #for trouble-shooting
        p1.wait()

    #return to old directory
    os.chdir(old_dir)
    
    #check if database creation was successful
    for fasta_file in os.listdir(fasta_dir_path):
        db_name = os.path.splitext(fasta_file)[0]+"_DB"
        db_created = False
        for db_file_name in os.listdir(blastdb_path):
            if(db_name in db_file_name):
                db_created = True
                continue
        if not db_created:
            sys.exit("Couldn´t create Database for "+fasta_file)

    print("Finished updating Database!")

#takes a gtf file and returns a dictionary containing the data
def create_dict_from_gtf(gtf_path):
    #list of features recorded in dict
    features = ["gene","CDS","5UTR","3UTR","ncRNA","pseudogene"]

    gtf_dict = {}

    with open(gtf_path, "r") as gtf_file:
        for line in gtf_file:
            if line.startswith("#"):
                continue
            line_split = line.split()
            feat_type = line_split[2]
            if feat_type in features:
                chromosome = line_split[0]
                strand = line_split[6]
                start, end = int(line_split[3]), int(line_split[4])

                #get transcript id
                transcript_id = "None"
                attributes = line_split[8].split(";")
                for att in attributes:
                    if("FBtr" in att):
                        transcript_id = att.split('"')[1]
                        break
                
                # project position onto smaller range
                start_pos = int((start) / PROJECTION_FACTOR)
                end_pos = int((end) / PROJECTION_FACTOR)
                
                #add empty dict for chromosome if necessary
                if not chromosome in gtf_dict:
                    gtf_dict[chromosome] = {}

                #add entry to dict
                for pos in range(start_pos, end_pos + 1):
                    #add empty list for chromosome,pos if necessary
                    if not pos in gtf_dict[chromosome]:
                        gtf_dict[chromosome][pos] = []

                    gtf_dict[chromosome][pos].append((start, end, feat_type, strand, transcript_id))
                    
    return gtf_dict

#given a chromosome, start/end of a sequence, sequence strand and a gtf dictionary
#determines annotation status
def get_annotation_status(blast_hit,sORF_strand,transcript_id,gtf_dict):
    overlapping_features = []
    overlapping_features_other_strand = [] #only stored right now, not used

    chromosome = blast_hit.sseqid
    start = blast_hit.sstart
    end = blast_hit.send
    blast_strand = blast_hit.sstrand

    start = int(start)
    end = int(end)
    strand = blast_strand if (blast_strand != "N/A") else sORF_strand

    #change +/- to 1/-1
    strand = "1" if (strand == "+") else strand
    strand = "-1" if (strand == "-") else strand

    start_pos = int((start) / PROJECTION_FACTOR)
    end_pos = int((end) / PROJECTION_FACTOR)
    #check for all entries of the chromosome positions if they overlap
    for pos in range(start_pos , end_pos +1 ):
        if gtf_dict.get(chromosome) == None:
            continue
        if gtf_dict[chromosome].get(pos) == None:
            continue
        for entry in gtf_dict[chromosome][pos]:
            if(overlap(start, end, entry[0], entry[1])):
                #check if transcript_id matches
                """
                if(not transcript_id == entry[4]):
                    print("Transcript ID mismatch!")
                    continue
                #"""
                #"""
                #change +/- to 1/-1
                dict_strand = entry[3]
                dict_strand = "1" if (dict_strand == "+") else dict_strand
                dict_strand = "-1" if (dict_strand == "-") else dict_strand
                #print("Strand stored in dict:{}, blast strand:{}, sORF_strand:{}".format(dict_strand,blast_strand,sORF_strand))
                if(dict_strand == strand):
                    overlapping_features.append(entry[2])   #if same strand
                else:
                    overlapping_features_other_strand.append(entry[2]) #if other strand
                #"""

    print("Transcript ID: {} overlapping features{}".format(transcript_id,overlapping_features))

    if(len(overlapping_features) > 0):
        #check if intron
        if("gene" in overlapping_features and len(set(["CDS","5UTR","3UTR"]).intersection(overlapping_features)) == 0 ):
            return "intron"
        #check CDS, 5UTR, 3UTR or pseudogene 
        for checked_feat in ["CDS","5UTR","3UTR","pseudogene"]:
            if(checked_feat in overlapping_features):
                if(checked_feat == "CDS"): #to match sORF annotation
                    return "exonic"
                else:
                    return checked_feat
    else:
        return "intergenic"

#checks if two sequences are overlapping
#Old Version
def overlapOld(start1, end1, start2, end2):
    if start1 >= start2 and start1 <= end2:
        return True
    if end1 >= start2 and end1 <= end2:
        return True
    return False

#checks if two sequences are overlapping
def overlap(start1, end1, start2, end2):
    if between(start1 ,start2,end2):
        return True
    if between(end1 ,start2,end2):
        return True
    return False

#checks if number x is between a and b
def between(x, a, b):
    if( a <= x and x <= b):
        return True
    if( b <= x and x <= a):
        return True
    return False

#returns distance of 2 sequences, assuming theyre on the same chromosome
def get_distance_on_chromosome(start1, end1, start2, end2):
    if overlap(start1, end1, start2, end2):
        return 0
    dist1 = abs(start1-end2)
    dist2 = abs(start2-end1)
    return min(dist1,dist2)

#TODO working correctly ?
#checks annotation status of all entries of an sORF file
#Method: performs a blast search, checks annotation of best blast result from a given gtf file
#sORF file needs to contain: Sorf ID, Chromosome, strand, AA sequence,sequence start/end and annotation
def check_file_annotation(file,gtf_path,error_log,database_name,evalue):
    init_start = time.time()
    firstLine = next(file)
    line_length = num_used_att(firstLine)
    chrom_pos = findColumnPos(firstLine,'Chromosome')
    start_pos = findColumnPos(firstLine,'Sorf start')
    end_pos = findColumnPos(firstLine,'Sorf end')
    ann_pos = findColumnPos(firstLine,'Annotation')
    id_pos = findColumnPos(firstLine,"Sorf ID")
    seq_pos = findColumnPos(firstLine,"AA sequence")
    strand_pos = findColumnPos(firstLine,"Strand")
    transcript_id_pos = findColumnPos(firstLine,'Ensembl transcript ID')

    print("Creating dictionary from gtf file...")
    gtf_dict = create_dict_from_gtf(gtf_path)
    print("Finished creating dictionary")
    #go into temp directory
    old_dir = os.getcwd()
    os.chdir(os.path.join(this_path,"temp"))

    lines = 0
    ann_err = 0
    bad_lines = 0
    init_end = time.time()
    init_sum = init_end-init_start
    line_check_sum = 0
    create_file_sum = 0
    blast_sum = 0
    ann_check_sum = 0
    for nextLine in file:
        line_check_start = time.time()
        lines += 1
        if(lines%5==0):
            print("Checking annotation of line {}...".format(lines))
        lineArr = nextLine.split()
        repair_lineArr(lineArr,line_length,firstLine)

        #Attribute missing / format of line ok ?
        if len(lineArr) != line_length:
            error_log.write((" ".join(lineArr) + "\n| Too many/little columns present ("+ str(len(lineArr)) +"). "
                "Number of columns in line should match number of attributes ("+ str(line_length) +") in first Line |\n\n"))
            bad_lines += 1
            continue

        chromosome = lineArr[chrom_pos]
        start = lineArr[start_pos]
        end = lineArr[end_pos]
        annotation = lineArr[ann_pos]
        sorf_id = lineArr[id_pos]
        sequence = lineArr[seq_pos]
        strand = lineArr[strand_pos]
        transcript_id = lineArr[transcript_id_pos]

        #Typecheck
        if not (typecheck(chromosome,str) and typecheck(start,int) and typecheck(sequence,str)
            and typecheck(end,int) and typecheck(annotation,str) and typecheck(sorf_id,str)
            and typecheck(strand,int) and typecheck(transcript_id,str)):
            error_log.write(" ".join(lineArr) + "\n| An attribute has wrong datatype |\n\n")
            bad_lines += 1
            continue
        line_check_end = time.time()
        line_check_sum += line_check_end-line_check_start

        create_file_start = time.time()
        #create query fasta file
        tmp_path = os.path.join(this_path,"temp","tmp.fasta")
        if(os.path.isfile(tmp_path)):
            os.remove(tmp_path)#remove tmp file 
        tmp = open("tmp.fasta","w")
        tmp.write(">"+sorf_id+"\n")
        sequenceArr = [ sequence[i:i+75] for i in range(0, len(sequence), 75) ]
        for line in sequenceArr:
            tmp.write(line+"\n")
        tmp.close()
        create_file_end = time.time()
        create_file_sum += create_file_end-create_file_start

        blast_start = time.time()
        blast_result = tblastn(tmp_path,database_name,evalue)
        blast_end = time.time()
        blast_sum += blast_end-blast_start

        #check if no blast results
        if(len(blast_result)==0):
            error_log.write((" ".join(lineArr) + "\n| Blast search didnt return any results (with given evalue)"+
                 "for this line |\n\n"))
            ann_err += 1
            continue

        #get closest blast result
        #set first hit as closest
        closest_blast_hit = blast_result[0]
        min_distance = get_distance_on_chromosome(int(start),int(end),int(closest_blast_hit.sstart),int(closest_blast_hit.send))
        #search rest of blast results
        #"""
        for hit in blast_result:
            if(hit.sseqid == chromosome):
                if(get_distance_on_chromosome(int(start),int(end),int(hit.sstart),int(hit.send)) < min_distance):
                    closest_blast_hit = hit
                    min_distance = get_distance_on_chromosome(int(start),int(end),int(hit.sstart),int(hit.send))
        #"""

        ann_check_start = time.time()
        if(closest_blast_hit.sstrand == "N/A"):
            if(closest_blast_hit.sstart < closest_blast_hit.send):
                closest_blast_hit.sstrand = "1"
            else:
                closest_blast_hit.sstrand = "-1"
        gtf_ann = get_annotation_status(closest_blast_hit, strand, transcript_id, gtf_dict)
        #print(closest_blast_hit.sstrand)
        ann_check_end = time.time()
        ann_check_sum += ann_check_end-ann_check_start

        if(gtf_ann != annotation):
            obj = closest_blast_hit
            blast_hit_string = ("{} {} {} {} {} {} {} {} {} {} {} {} {}".format(obj.qseqid,obj.sseqid,obj.pident,obj.length,
            obj.mismatch,obj.gapopen,obj.qstart,obj.qend,obj.sstart,obj.send,obj.scr,obj.sstrand,obj.bitscore.rstrip()))
            error_log.write((" ".join(lineArr) + "\n" + blast_hit_string +
                ("\n| Annotation mismatch: sORF={}"+
                " tblastn&gtf={} |\n\n").format(annotation,gtf_ann)))
            ann_err += 1
            continue
    print("Annotation check of file done!")
    #''' --- Print time taken for steps of annotation_check
    print("""
    Init time: {}s\n
    time checking lines: {}s\n
    time writing fasta files: {}s\n
    time blast searching: {}s\n
    time checking annotation: {}s\n""".format(init_sum,line_check_sum,create_file_sum,blast_sum,ann_check_sum))
    #'''

    #return to old directory
    os.chdir(old_dir)

    error_log.write("{} out of {} lines have not been processed due to faulty formatting({}%)\n".format(bad_lines,lines,(bad_lines/lines)*100))
    error_log.write("{} out of {} annotations could not be verified({}%)".format(ann_err,(lines-bad_lines),(ann_err/(lines-bad_lines))*100))

#function definitions [END]

#Test-Area

#tblastn(os.path.join(this_path,"temp","tmp.fasta"),"dmel-all-chromosome-r6.24_DB",1)
"""
result = tblastn(os.path.join(this_path,"temp","tmp.fasta"),"dmel-all-chromosome-r6.24_DB",1)
best_hit = result[0]
for obj in result:
    print("{} {} {} {} {} {} {} {} {} {} {} {}".format(obj.qseqid,obj.sseqid,obj.pident,obj.length,
        obj.mismatch,obj.gapopen,obj.qstart,obj.qend,obj.sstart,obj.send,obj.scr,obj.bitscore))
#"""

"""
gtf_path = os.path.join(this_path,"Drosophila_Data","Drosophila_melanogaster","dmel-all-r6.24.gtf")
gtf_dict = create_dict_from_gtf(gtf_path)
print("Hit to be searched: Chromosome={} Start={} End={}".format(best_hit.sseqid, best_hit.sstart, best_hit.send))
print(get_annotation_status(best_hit.sseqid, best_hit.sstart, best_hit.send, gtf_dict))
#"""

